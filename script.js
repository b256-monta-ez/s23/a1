// console.log("Hey");

let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
				hoenn: ['May', 'Max'],
				kanto: ['Brock', 'Misty']
			}

};
console.log(trainer);

trainer.talk = function () {
	console.log('Pikachu! I choose You!');
}

console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

console.log('Result of talk method')
trainer.talk();

function pokemon(name, level, health, attack) {
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;


}

let pikachu = new pokemon("Pikachu", 12, 24, 12);
// console.log(pikachu);
let geodude = new pokemon("Geodude", 8, 16, 8);
// console.log(geodude);
let mewTwo = new pokemon('Mewtwo', 100, 200, 100);
// console.log(mewTwo);

// PIKACHU METHODS
pikachu.tackle = function(pokemonsTurn, targetPokemon) {
	console.log('Pikachu used tackle to Geodude');
	console.log("Geodude's health is down to: " + (targetPokemon - pokemonsTurn));
	geodude.health = targetPokemon - pokemonsTurn;
	console.log(geodude);
}

pikachu.faint = function(pokemonsTurn, targetPokemon) {
	if(pikachu.health <= 0) {
		console.log("Pikachu fainted")
		console.log(pikachu);
	}
}

// MEWTWO METHODS

mewTwo.tackle = function(pokemonsTurn, targetPokemon) {
	console.log('MewTwo used tackle to Pikachu');
	pikachu.health = targetPokemon - pokemonsTurn;
	console.log("Pikachu's health is down to: " + pikachu.health);
}
mewTwo.faint = function(pokemonsTurn, targetPokemon) {
	if(mewTwo.health <= 0) {
		console.log("MewTwo fainted")
	}
}


// GEODUDE METHODS

geodude.tackle = function(pokemonsTurn, targetPokemon) {
	console.log('Geodude used tackle to MewTwo');
	console.log("MewTwo's health is down to: " + (targetPokemon - pokemonsTurn));
	mewTwo.health = targetPokemon - pokemonsTurn;
}
geodude.faint = function(pokemonsTurn, targetPokemon) {
	if(geodude.health <= 0) {
		console.log("Pikachu fainted")
	}
}

// INVOCATION
console.log(pikachu);
console.log(geodude);
console.log(mewTwo);

pikachu.tackle(pikachu.attack, geodude.health);
mewTwo.tackle(mewTwo.attack, pikachu.health);
pikachu.faint();